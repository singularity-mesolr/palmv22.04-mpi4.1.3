# palmv22.04-mpi4.1.3

Container for ACTA with palm working with openmpi4.1.3

## CONTENT

The singularity image is delivered with =>
```
palm_model_system-v22.04
openmpio-4.1.3
```
## BUILD
```
singularity build palmv22.04-mpi4.1.3.sif palmv22.04-mpi4.1.3.def
```
## USAGE
First, pull the image then use it like so =>
```
singularity pull palmv22.04-mpi4.1.3.sif oras://registry.forgemia.inra.fr/singularity-mesolr/palmv22.04-mpi4.1.3/palmv22.04-mpi4.1.3:latest
./palmv22.04-mpi4.1.3.sif palmrun ...
```
